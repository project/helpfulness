<?php

namespace Drupal\helpfulness\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\helpfulness\Form\HelpfulnessBlockForm;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Helpfulness' block.
 *
 * @Block(
 *   id = "helpfulness_block",
 *   admin_label = @Translation("Helpfulness block"),
 * )
 */
class HelpfulnessBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Open state (unprocessed) for feedback entries.
   *
   * @var int
   */
  const HELPFULNESS_STATUS_OPEN = 1;

  /**
   * Archived state for feedback entries.
   *
   * @var int
   */
  const HELPFULNESS_STATUS_ARCHIVED = 2;

  /**
   * Deleted state for feedback entries.
   *
   * @var int
   */
  const  HELPFULNESS_STATUS_DELETED = 3;

  /**
   * A form builder service instance.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->formBuilder = $container->get('form_builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->formBuilder->getForm(HelpfulnessBlockForm::class);

    return $form;
  }

}
