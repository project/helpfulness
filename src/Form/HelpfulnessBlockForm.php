<?php

namespace Drupal\helpfulness\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\helpfulness\Plugin\Block\HelpfulnessBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a block form to leave helpfulness feedback.
 */
class HelpfulnessBlockForm extends FormBase {

  /**
   * A database connection service instance.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * A time service instance.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $time;

  /**
   * A current path service instance.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * A plugin manager mail service instance.
   *
   * @var \Drupal\Core\Mail\MailManager
   */
  protected $mailManager;

  /**
   * A language manager service instance.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    $instance->time = $container->get('datetime.time');
    $instance->currentPath = $container->get('path.current');
    $instance->mailManager = $container->get('plugin.manager.mail');
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'helpfulness_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Include the css.
    $form['#attached']['library'] = [
      'helpfulness/helpfulness-block-form',
    ];

    $form['helpfulness_rating'] = [
      '#type' => 'radios',
      '#options' => [1 => $this->t('Yes'), 0 => $this->t('No')],
    ];

    $config = $this->config('helpfulness.settings');

    // If comments are allowed when the user selects "Yes".
    if ($config->get('helpfulness_yes_allow_comment')) {
      $title_yes = '<div class="helpfulness_yes_title">' . $config->get('helpfulness_yes_title') . '</div>';
      $description_yes = '<div class="helpfulness_yes_description">' . $config->get('helpfulness_yes_description') . '</div >';

      $form['helpfulness_comments_yes'] = [
        '#type' => 'textarea',
        '#title' => $title_yes,
        '#description' => $description_yes,
      ];
    }

    // If comments are allowed when the user selects "No".
    if ($config->get('helpfulness_no_allow_comment')) {
      $title_no = '<div class="helpfulness_no_title">' . $config->get('helpfulness_no_title') . '</div >';
      $description_no = '<div class="helpfulness_no_description">' . $config->get('helpfulness_no_description') . '</div >';

      $form['helpfulness_comments_no'] = [
        '#type' => 'textarea',
        '#title' => $title_no,
        '#description' => $description_no,
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('helpfulness.settings');

    if ($form_state->getValue('helpfulness_rating') === NULL) {
      $form_state->setErrorByName('helpfulness_rating', $this->t('Please indicate if this page is helpful or not.'));
    }

    elseif ($form_state->getValue('helpfulness_rating') == 1 && $config->get('helpfulness_yes_allow_comment') && $config->get('helpfulness_yes_comment_required')) {
      $comment_text_yes = strip_tags($form_state->getValue('helpfulness_comments_yes'));
      if (strlen($comment_text_yes) < 1) {
        $form_state->setErrorByName('helpfulness_comments_yes', $config->get('helpfulness_yes_comment_required_message'));
      }
    }

    elseif ($form_state->getValue('helpfulness_rating') == 0 && $config->get('helpfulness_no_allow_comment') && $config->get('helpfulness_no_comment_required')) {
      $comment_text_no = strip_tags($form_state->getValue('helpfulness_comments_no'));
      if (strlen($comment_text_no) < 1) {
        $form_state->setErrorByName('helpfulness_comments_no', $config->get('helpfulness_no_comment_required_message'));
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the useragent, with a fall back in case the access is blocked.
    if ($user_agent = $this->getRequest()->server->get('HTTP_USER_AGENT')) {
      $user_agent = Html::escape(trim($user_agent));
    }
    else {
      $user_agent = $this->t('not available');
    }

    // Validate the comments.
    if (!empty($form_state->getValue('helpfulness_comments_yes'))) {
      $comments = mb_substr(trim(strip_tags($form_state->getValue('helpfulness_comments_yes'))), 0, 1024);
    }
    if (!empty($form_state->getValue('helpfulness_comments_no'))) {
      $comments = mb_substr(trim(strip_tags($form_state->getValue('helpfulness_comments_no'))), 0, 1024);
    }
    if (empty($comments)) {
      $comments = $this->t('None');
    }

    // Get the authenticated user ID (if any).
    $user_id = $this->currentUser()->id();

    // Form values.
    $fields = [
      'uid' => $user_id,
      'status' => HelpfulnessBlock::HELPFULNESS_STATUS_OPEN,
      'system_path' => $this->currentPath->getPath(),
      'path_alias' => Url::fromRoute("<current>")->toString(),
      'base_url' => $this->getRequest()->getSchemeAndHttpHost(),
      'helpfulness' => $form_state->getValue('helpfulness_rating'),
      'message' => $comments,
      'useragent' => $user_agent,
      'timestamp' => $this->time->getRequestTime(),
    ];

    // Insert the data to the helpfulness table.
    $this->database->insert('helpfulness')
      ->fields($fields)
      ->execute();

    // Get the configuration.
    $config = $this->config('helpfulness.settings');
    $notification_email = $config->get('helpfulness_notification_email');

    if (!empty($notification_email)) {
      $params['helpfulness_rating'] = $form_state->getValue('helpfulness_rating');
      if (!empty($form_state->getValue('helpfulness_comments_yes'))) {
        $params['helpfulness_comments'] = $form_state->getValue('helpfulness_comments_yes');
      };
      if (!empty($form_state->getValue('helpfulness_comments_no'))) {
        $params['helpfulness_comments'] = $form_state->getValue('helpfulness_comments_no');
      }
      $params['page_url'] = $fields['base_url'] . $fields['path_alias'];

      // Get the site email address.
      $site_mail = $this->config('system.site')->get('mail');

      // Default language of the site.
      $language = $this->languageManager->getDefaultLanguage();

      // Send the email.
      $this->mailManager->mail('helpfulness', 'new_feedback_notification', $notification_email, $language, $params, $site_mail);
    }

    $this->messenger()->addMessage($this->t('Thank you for your feedback.'));
  }

}
