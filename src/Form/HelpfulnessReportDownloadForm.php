<?php

namespace Drupal\helpfulness\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\helpfulness\Plugin\Block\HelpfulnessBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for the report download.
 */
class HelpfulnessReportDownloadForm extends FormBase {

  /**
   * A database connection service instance.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * A date formatter service instance.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * An entity type manager service instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'helpfulness_report_download_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Status selection.
    $form['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Download messages of status:'),
      '#options' => [
        '-1' => $this->t('New & Archived'),
        HelpfulnessBlock::HELPFULNESS_STATUS_OPEN => $this->t('New'),
        HelpfulnessBlock::HELPFULNESS_STATUS_ARCHIVED => $this->t('Archived'),
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    // Get the status from the dropdown.
    $requested_status = $values['status'];

    // Build the query to retrieve the feedbacks.
    // Build the query.
    $query = $this->database->select('helpfulness', 'hf')
      ->fields('hf')
      ->orderBy('timestamp', 'ASC');

    switch ($requested_status) {
      case HelpfulnessBlock::HELPFULNESS_STATUS_OPEN:
        $query->condition('status', HelpfulnessBlock::HELPFULNESS_STATUS_OPEN);
        break;

      case HelpfulnessBlock::HELPFULNESS_STATUS_ARCHIVED:
        $query->condition('status', HelpfulnessBlock::HELPFULNESS_STATUS_ARCHIVED);
        break;

      default:
        $status_array[] = HelpfulnessBlock::HELPFULNESS_STATUS_OPEN;
        $status_array[] = HelpfulnessBlock::HELPFULNESS_STATUS_ARCHIVED;
        $query->condition('status', $status_array, 'IN');
        break;
    }

    // Header for the output file.
    $csv_output = $this->t('"Status",');
    $csv_output .= $this->t('"User ID",');
    $csv_output .= $this->t('"Helpful",');
    $csv_output .= $this->t('"Message",');
    $csv_output .= $this->t('"Base URL",');
    $csv_output .= $this->t('"System Path",');
    $csv_output .= $this->t('"Path Alias",');
    $csv_output .= $this->t('"Browser Information",');
    $csv_output .= $this->t('"Time"');
    $csv_output .= "\n";

    // Add the data from all requested feedbacks.
    foreach ($query->execute() as $row) {
      // Status.
      switch ($row->status) {
        case HelpfulnessBlock::HELPFULNESS_STATUS_OPEN:
          $csv_output .= $this->t('"New",');
          break;

        case HelpfulnessBlock::HELPFULNESS_STATUS_ARCHIVED:
          $csv_output .= $this->t('"Archived",');
          break;

        default:
          $csv_output .= $this->t('"Unknown",');
          break;
      }

      // User Id, and user name for convenience.
      if ($row->uid == 0) {
        $username = $this->t('Anonymous');
      }
      else {
        /** @var \Drupal\user\Entity\User|null $tmp_user */
        $tmp_user = $this->entityTypeManager->getStorage('user')->load($row->uid);
        $username = $tmp_user ? $tmp_user->getDisplayName() : $this->t('Deleted');
      }
      $csv_output .= '"' . $row->uid . ' (\'' . $username . '\')",';

      // Helpfulnes Rating.
      if ($row->helpfulness) {
        $csv_output .= $this->t('"Yes",');
      }
      else {
        $csv_output .= $this->t('"No",');
      }

      // Feedback message.
      $message = str_replace('"', '""', $row->message);
      $csv_output .= '"' . $message . '",';

      // Path information.
      $csv_output .= '"' . $row->base_url . '",';
      $csv_output .= '"' . $row->system_path . '",';
      $csv_output .= '"' . $row->path_alias . '",';

      // Browser info.
      $csv_output .= '"' . $row->useragent . '",';

      // Time of submission.
      $csv_output .= '"' . $this->dateFormatter->format($row->timestamp, 'custom', "Y-m-d H:i:s") . '",';

      // That should be everything for this submission.
      $csv_output .= "\n";
    }

    // Build the filename and start the download.
    $prefix = $this->t('feedbacks');
    $filename = $prefix . "_" . $this->dateFormatter->format(time(), 'custom', "Y-m-d_His");
    header("Content-type: application/vnd.ms-excel");
    header("Content-disposition: filename=" . $filename . ".csv");
    print $csv_output;

    exit();
  }

}
