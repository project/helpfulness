<?php

namespace Drupal\helpfulness\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for the administration values.
 */
class HelpfulnessAdminForm extends ConfigFormBase {

  /**
   * An email validator service instance.
   *
   * @var \Drupal\Component\Utility\EmailValidator
   */
  protected $emailValidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->emailValidator = $container->get('email.validator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'helpfulness_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['helpfulness.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get the configuration.
    $config = $this->config('helpfulness.settings');

    // Options if the user selected "Yes".
    $form['yes_options'] = [
      '#type' => 'details',
      '#title' => $this->t('"YES" Options'),
    ];

    $form['yes_options']['helpfulness_yes_allow_comment'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show comment box'),
      '#default_value' => $config->get('helpfulness_yes_allow_comment'),
    ];

    $form['yes_options']['helpfulness_yes_title'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Title for the comments'),
      '#default_value' => $config->get('helpfulness_yes_title'),
      '#description' => $this->t('Markup for the title if the user selects "yes". This text will appear above the comments area.'),
      '#states' => [
        'visible' => [
          ':input[name="helpfulness_yes_allow_comment"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['yes_options']['helpfulness_yes_description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description for the comments'),
      '#default_value' => $config->get('helpfulness_yes_description'),
      '#description' => $this->t('Markup for the description if the user selects "yes". This text will appear below the comments area'),
      '#states' => [
        'visible' => [
          ':input[name="helpfulness_yes_allow_comment"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['yes_options']['helpfulness_yes_message_required'] = [
      '#type' => 'details',
      '#title' => $this->t('Are comments required to submit feedback?'),
      '#states' => [
        'visible' => [
          ':input[name="helpfulness_yes_allow_comment"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['yes_options']['helpfulness_yes_message_required']['helpfulness_yes_comment_required'] = [
      '#type' => 'radios',
      '#title' => $this->t('Status'),
      '#default_value' => $config->get('helpfulness_yes_comment_required') ?? 0,
      '#options' => [0 => $this->t('No'), 1 => $this->t('Yes')],
    ];

    $form['yes_options']['helpfulness_yes_message_required']['helpfulness_yes_comment_required_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message Text'),
      '#default_value' => $config->get('helpfulness_yes_comment_required_message'),
      '#description' => $this->t('Text for error message if feedback was submitted without a comment.'),
      '#states' => [
        'visible' => [
          ':input[name="helpfulness_yes_comment_required"]' => ['value' => 1],
        ],
      ],
    ];

    // Options if the user selected "No".
    $form['no_options'] = [
      '#type' => 'details',
      '#title' => $this->t('"NO" Options'),
    ];

    $form['no_options']['helpfulness_no_allow_comment'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show comment box'),
      '#default_value' => $config->get('helpfulness_no_allow_comment'),
    ];

    $form['no_options']['helpfulness_no_title'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Title for the comments'),
      '#default_value' => $config->get('helpfulness_no_title'),
      '#description' => $this->t('Markup for the title if the user selects "no". This text will appear above the comments area.'),
      '#states' => [
        'visible' => [
          ':input[name="helpfulness_no_allow_comment"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['no_options']['helpfulness_no_description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description for the comments'),
      '#default_value' => $config->get('helpfulness_no_description'),
      '#description' => $this->t('Markup for the description if the user selects "no". This text will appear below the comments area'),
      '#states' => [
        'visible' => [
          ':input[name="helpfulness_no_allow_comment"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    // Options for the comments.
    $form['no_options']['helpfulness_no_message_required'] = [
      '#type' => 'details',
      '#title' => $this->t('Are comments required to submit feedback?'),
      '#states' => [
        'visible' => [
          ':input[name="helpfulness_no_allow_comment"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['no_options']['helpfulness_no_message_required']['helpfulness_no_comment_required'] = [
      '#type' => 'radios',
      '#title' => $this->t('Status'),
      '#default_value' => $config->get('helpfulness_no_comment_required') ?? 0,
      '#options' => [0 => $this->t('No'), 1 => $this->t('Yes')],
    ];

    $form['no_options']['helpfulness_no_message_required']['helpfulness_no_comment_required_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message Text'),
      '#default_value' => $config->get('helpfulness_no_comment_required_message'),
      '#description' => $this->t('Text for error message if feedback was submitted without a comment.'),
      '#states' => [
        'visible' => [
          ':input[name="helpfulness_no_comment_required"]' => ['value' => 1],
        ],
      ],
    ];

    // Options for notification emails.
    $form['email_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Email options'),
    ];

    $form['email_options']['helpfulness_notification_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#default_value' => $config->get('helpfulness_notification_email'),
      '#description' => $this->t('Enter an email to receive notifications if new feedback has been submitted.<br>If no email address is given then no notification email will be send.'),
    ];

    $form['email_options']['helpfulness_notification_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('helpfulness_notification_subject'),
      '#description' => $this->t('Subject for the notification email.'),
    ];

    $form['email_options']['helpfulness_notification_message_prefix'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prefix for message body'),
      '#default_value' => $config->get('helpfulness_notification_message_prefix'),
      '#description' => $this->t('Text for the body of the notification email, will be prepended to values submitted by the user.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $notification_email = trim($form_state->getValue('helpfulness_notification_email'));
    if (!empty($notification_email) && !$this->emailValidator->isValid($notification_email)) {
      $form_state->setErrorByName('helpfulness_notification_email', $this->t('The email address you entered is not valid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('helpfulness.settings');

    $config->set('helpfulness_yes_title', $form_state->getValue('helpfulness_yes_title')['value'])
      ->set('helpfulness_yes_description', $form_state->getValue('helpfulness_yes_description')['value'])
      ->set('helpfulness_no_title', $form_state->getValue('helpfulness_no_title')['value'])
      ->set('helpfulness_no_allow_comment', $form_state->getValue('helpfulness_no_allow_comment'))
      ->set('helpfulness_yes_allow_comment', $form_state->getValue('helpfulness_yes_allow_comment'))
      ->set('helpfulness_no_description', $form_state->getValue('helpfulness_no_description')['value'])
      ->set('helpfulness_yes_comment_required', $form_state->getValue('helpfulness_yes_comment_required'))
      ->set('helpfulness_yes_comment_required_message', $form_state->getValue('helpfulness_yes_comment_required_message'))
      ->set('helpfulness_no_comment_required', $form_state->getValue('helpfulness_no_comment_required'))
      ->set('helpfulness_no_comment_required_message', $form_state->getValue('helpfulness_no_comment_required_message'))
      ->set('helpfulness_notification_email', $form_state->getValue('helpfulness_notification_email'))
      ->set('helpfulness_notification_subject', $form_state->getValue('helpfulness_notification_subject'))
      ->set('helpfulness_notification_message_prefix', $form_state->getValue('helpfulness_notification_message_prefix'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
