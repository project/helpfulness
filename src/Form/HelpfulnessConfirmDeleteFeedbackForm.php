<?php

namespace Drupal\helpfulness\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\helpfulness\Plugin\Block\HelpfulnessBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to confirm the deletion of a feedback submission.
 */
class HelpfulnessConfirmDeleteFeedbackForm extends ConfirmFormBase {

  /**
   * The array of IDs of the items to delete.
   *
   * @var array
   */
  protected $ids;

  /**
   * A database connection service instance.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'helpfulness_confirm_delete_feedback_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to delete the feedbacks?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('helpfulness.report_form');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    if (count($this->ids) > 1) {
      return $this->t('Are you sure that you want to delete the selected %count feedback items?', ['%count' => count($this->ids)]);
    }
    return $this->t('Are you sure that you want to delete the selected feedback item?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Yes, delete!');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $idstring = NULL) {
    $this->ids = explode('-', $idstring);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($this->ids)) {
      // Build the update query and execute.
      /** @var \Drupal\Core\Database\Query\Update $query */
      $query = $this->database->update('helpfulness')
        ->fields(['status' => HelpfulnessBlock::HELPFULNESS_STATUS_DELETED])
        ->condition('fid', $this->ids, 'IN');
      $query->execute();
    }

    $this->messenger()->addMessage($this->t('The selected feedbacks have been deleted.'));
    $form_state->setRedirect('helpfulness.report_form');
  }

}
