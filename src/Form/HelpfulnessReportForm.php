<?php

namespace Drupal\helpfulness\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Query\TableSortExtender;
use Drupal\helpfulness\Plugin\Block\HelpfulnessBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form showing the submitted feedbacks.
 */
class HelpfulnessReportForm extends FormBase {

  /**
   * A database connection service instance.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * A date formatter service instance.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * An entity type manager service instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'helpfulness_report_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Include the css.
    $form['#attached']['library'] = ['helpfulness/helpfulness-block-form'];

    $config = $this->config('helpfulness.settings');

    // Build the array for the header of the table.
    $header = [];
    if ($config->get('helpfulness_report_option_display_username')) {
      $header['uid'] = [
        'data' => $this->t('User'),
        'field' => 'uid',
      ];
    }
    if ($config->get('helpfulness_report_option_display_helpfulness')) {
      $header['helpfulness'] = [
        'data' => $this->t('Helpful'),
        'field' => 'helpfulness',
      ];
    }
    if ($config->get('helpfulness_report_option_display_message')) {
      $header['message'] = [
        'data' => $this->t('Message'),
        'field' => 'message',
      ];
    }
    if ($config->get('helpfulness_report_option_display_base_url')) {
      $header['base_url'] = [
        'data' => $this->t('Base URL'),
        'field' => 'base_url',
      ];
    }
    if ($config->get('helpfulness_report_option_display_system_path')) {
      $header['system_path'] = [
        'data' => $this->t('System Path'),
        'field' => 'system_path',
      ];
    }
    if ($config->get('helpfulness_report_option_display_alias')) {
      $header['path_alias'] = [
        'data' => $this->t('Alias'),
        'field' => 'path_alias',
      ];
    }
    if ($config->get('helpfulness_report_option_display_date')) {
      $header['date'] = [
        'data' => $this->t('Date'),
        'field' => 'timestamp',
      ];
    }
    if ($config->get('helpfulness_report_option_display_time')) {
      $header['time'] = $this->t('Time');
    }
    if ($config->get('helpfulness_report_option_display_useragent')) {
      $header['useragent'] = [
        'data' => $this->t('Browser'),
        'field' => 'useragent',
      ];
    }

    // Setting the sort conditions.
    $query = $this->getRequest()->query;
    if ($query->get('sort') && $query->get('order')) {
      // Sort it Ascending or Descending?
      if ($query->get('sort') === 'asc') {
        $sort = 'ASC';
      }
      else {
        $sort = 'DESC';
      }

      // Which column will be sorted.
      switch ($query->get('order')) {
        case 'User ID':
          $order = 'uid';
          break;

        case 'Helpful':
          $order = 'helpfulness';
          break;

        case 'Message':
          $order = 'message';
          break;

        case 'Base URL':
          $order = 'base_url';
          break;

        case 'System Path':
          $order = 'system_path';
          break;

        case 'Alias':
          $order = 'path_alias';
          break;

        case 'Date':
          $order = 'timestamp';
          break;

        case 'Browser':
          $order = 'useragent';
          break;

        default:
          $order = 'timestamp';
      }
    }
    else {
      $sort = 'ASC';
      $order = 'timestamp';
    }

    // For all feedbacks retrieved from the db.
    $feedbacks = [];

    // Build the query.
    /** @var \Drupal\Core\Database\Query\TableSortExtender $query */
    $query = $this->database->select('helpfulness', 'hf')
      ->fields('hf')
      ->orderBy($order, $sort)
      ->extend(TableSortExtender::class);

    // Fetch all results from the db.
    foreach ($query->execute() as $row) {
      $feedbacks[] = [
        'fid' => $row->fid,
        'status' => $row->status,
        'uid' => $row->uid,
        'helpfulness' => $row->helpfulness,
        'message' => $row->message,
        'useragent' => $row->useragent,
        'timestamp' => $row->timestamp,
        'system_path' => $row->system_path,
        'path_alias' => $row->path_alias,
        'base_url' => $row->base_url,
      ];
    }

    // Build the rows for the table.
    $options_open = [];
    $options_archived = [];

    foreach ($feedbacks as $feedback) {
      $option = [];

      /** @var \Drupal\user\Entity\User|null $tmp_user */
      $tmp_user = $this->entityTypeManager->getStorage('user')->load($feedback['uid']);
      $username = $tmp_user ? $tmp_user->getDisplayName() : $this->t('Deleted');

      $option['uid'] = $feedback['uid'] . ' (' . $username . ')';
      $option['helpfulness'] = ($feedback['helpfulness']) ? 'Yes' : 'No';
      $option['message'] = [
        'data' => [
          '#markup' => str_replace("\n", '<br>', Html::escape($feedback['message'])),
        ],
      ];

      $option['system_path'] = [
        'data' => [
          '#type' => 'link',
          '#title' => $feedback['system_path'],
          '#url' => Url::fromUserInput($feedback['system_path']),
        ],
      ];

      $option['path_alias'] = [
        'data' => [
          '#type' => 'link',
          '#title' => $feedback['path_alias'],
          '#url' => Url::fromUserInput($feedback['path_alias']),
        ],
      ];

      $option['base_url'] = $feedback['base_url'];
      $option['date'] = $this->dateFormatter->format($feedback['timestamp'], 'custom', 'Y-m-d');
      $option['time'] = $this->dateFormatter->format($feedback['timestamp'], 'custom', 'H:i');
      $option['useragent'] = [
        'data' => [
          '#markup' => $feedback['useragent'],
          '#prefix' => '<div class="useragent_description">',
          '#suffix' => '</div>',
        ],
      ];

      // Add this feedback to the appropriate table.
      switch ($feedback['status']) {
        case HelpfulnessBlock::HELPFULNESS_STATUS_OPEN:
          $options_open[$feedback['fid']] = $option;
          break;

        case HelpfulnessBlock::HELPFULNESS_STATUS_ARCHIVED:
          $options_archived[$feedback['fid']] = $option;
          break;

      }

    }

    // Update actions.
    $options = [
      '' => $this->t('Please Select...'),
      HelpfulnessBlock::HELPFULNESS_STATUS_OPEN => $this->t('Set as "new"'),
      HelpfulnessBlock::HELPFULNESS_STATUS_ARCHIVED => $this->t('Archive'),
    ];

    // If the user has permissions to delete feedbacks add that option as well.
    if ($this->currentUser()->hasPermission('delete feedback')) {
      $options += [HelpfulnessBlock::HELPFULNESS_STATUS_DELETED => $this->t('Delete')];
    }

    $form['update_action'] = [
      '#type' => 'select',
      '#title' => $this->t('Update options:'),
      '#options' => $options,
      '#required' => TRUE,
    ];

    // Submit Button.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
    ];

    // Add the tables for open feedbacks to the form.
    $form['helpfulness_feedback_open'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('New feedback'),
    ];

    $form['helpfulness_feedback_open']['helpfulness_feedbacks_open_table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options_open,
      '#empty' => $this->t('No new feedback found'),
    ];

    // Add the tables for archived feedbacks to the form.
    $form['helpfulness_feedbacks_archived'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Archived feedback'),
    ];

    $form['helpfulness_feedbacks_archived']['helpfulness_feedbacks_archived_table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options_archived,
      '#empty' => $this->t('No archived feedback found'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    $open_fids = array_diff($values['helpfulness_feedbacks_open_table'], ["0"]);
    $archived_fids = array_diff($values['helpfulness_feedbacks_archived_table'], ["0"]);

    if (empty($open_fids) && empty($archived_fids)) {
      $this->messenger()->addMessage($this->t('Please select the items you would like to update.'), 'error');
      return;
    }

    $action = $values['update_action'];

    if ($action == HelpfulnessBlock::HELPFULNESS_STATUS_DELETED) {
      $id_string = implode('-', array_merge($open_fids, $archived_fids));
      $form_state->setRedirect('helpfulness.report_confirm_deletions_form',
        ['idstring' => $id_string]);
    }
    else {
      $this->helpfulnessProcessUpdateAction($action, $open_fids);
      $this->helpfulnessProcessUpdateAction($action, $archived_fids);
      $this->messenger()->addMessage($this->t('Your selected items have been updated.'));
    }

  }

  /**
   * Implements helpfulnessProcessUpdateAction().
   */
  private function helpfulnessProcessUpdateAction($action, $selected_fids) {
    if (empty($selected_fids)) {
      return;
    }

    // Build the update query and execute.
    /** @var \Drupal\Core\Database\Query\Update $query */
    $query = $this->database->update('helpfulness')
      ->fields(['status' => $action])
      ->condition('fid', $selected_fids, 'IN');
    $query->execute();
  }

}
